[TOC]

# 说明

> 一个基于SpringMVC的三层模块抽象封装，使得你的每一个功能模块CURD代码更精简。
> 配套基於spring-boot的maven工程：https://gitee.com/suze/base-module-demo

# 介绍

![架构示例](docs/base.png)

### `BaseMapper<E extends Serializable,PK,F extends Serializable>`

> DAO层的抽象封装，自己功能模块的Mapper需要继承该接口。

### `BaseService<E extends Serializable,P,F extends Serializable>`

> Service层的抽象封装，相应Service需要继承该接口。

### `BaseServiceImpl<E extends Serializable,P,F extends Serializable>`

> Service实现类的抽象封装，相应Service实现类需要继承该接口。

### `BaseFacade<V extends Serializable,E extends Serializable,P,F extends Serializable>`

> Facade层的抽象封装，相应Facade需要继承该接口。

### `BaseFacadeImpl<V extends Serializable,E extends Serializable,P extends Serializable,F extends Serializable>`

> Facade实现类的抽象封装，相应Facade实现类需要继承该接口。

### `BaseConverter<R extends Serializable, O extends Serializable>`

> 提供Entity对VO的转换，自己需要实现VO转换器并继承该抽象类。

### `PageForm<T>`

> 对分页查询功能的页面查询参数封装。

### `PageVO<T>`

> 对分页查询功能的数据结果集封装。

### `Result<E>`

> 对Restful API接口的响应封装。