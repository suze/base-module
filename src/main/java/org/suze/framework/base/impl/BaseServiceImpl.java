package org.suze.framework.base.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.suze.framework.base.BaseMapper;
import org.suze.framework.base.BaseService;
import org.suze.framework.base.page.PageForm;

import java.io.Serializable;
import java.util.List;

/**
 * @description: BaseService
 * @Date : 2019/5/31 下午2:25
 * @Author : 石冬冬-Seig Heil
 * @param <E> 数据库实体对象
 * @param <P> 查询主键
 * @param <F> 查询条件Form对象
 */
public abstract class BaseServiceImpl<E extends Serializable,P,F extends Serializable> implements BaseService<E,P,F> {
    /**
     * mapper
     */
    protected BaseMapper<E,P,F> mapper;

    @Autowired
    public void setBaseMapper(BaseMapper<E, P, F> baseMapper) {
        this.mapper = baseMapper;
    }

    @Override
    public List<E> queryByPage(PageForm<F> form) {
        return mapper.queryByPage(form);
    }

    @Override
    public int queryCount(PageForm<F> form) {
        return mapper.queryCount(form);
    }

    @Override
    public E queryRecord(P primaryKey) {
        return mapper.selectByPrimaryKey(primaryKey);
    }

    @Override
    public int insertRecord(E record) {
        return mapper.insert(record);
    }

    @Override
    public int updateRecord(E record) {
        return mapper.updateByPrimaryKey(record);
    }

    @Override
    public int updateByPrimaryKeySelective(E record) {
        return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int deleteRecord(P primaryKey) {
        return mapper.deleteByPrimaryKey(primaryKey);
    }

    @Override
    public int batchInsert(List<E> list) {
        return mapper.batchInsert(list);
    }

    @Override
    public int batchInsertIgnore(List<E> list) {
        return mapper.batchInsertIgnore(list);
    }

    @Override
    public int batchDelete(List<E> list) {
        return mapper.batchDelete(list);
    }

    @Override
    public int deleteByForm(F form) {
        return mapper.deleteByForm(form);
    }

    @Override
    public List<E> queryList(F form) {
        return mapper.queryList(form);
    }
}
