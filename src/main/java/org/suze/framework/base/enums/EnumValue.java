package org.suze.framework.base.enums;

/**
 * @description: 枚举接口，一切枚举应从通用扩展宜实现此接口
 * @Date : 2020/3/19 下午6:22
 * @Author : 石冬冬-Seig Heil
 */
public interface EnumValue {

    /**
     * 获取枚举索引
     * @return
     */
    int getIndex();

    /**
     * 获取枚举名称
     * @return
     */
    String getName();
}
