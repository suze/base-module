package org.suze.framework.base.enums;

/**
 * @description: 服务接口相关枚举
 * @Date : 下午3:47 2020/3/6
 * @Author : 石冬冬-Heil Hitler
 */
public enum RemoteEnum implements EnumValue{
    SUCCESS(0, "成功"),
    FAILURE(1, "失败"),
    ERROR_IN_RUNTIME(550, "运行时错误"),
    ERROR_IN_SERVER(551, "服务器内部错误"),
    ERROR_WITH_EMPTY_PARAM(552, "参数值不能为空"),
    WARN_EMPTY_RECORD(553, "未找到相关记录"),
    ILLEGAL_ARGUMENTS(554, "不合法参数"),
    ;

    private int index;
    private String name;

    RemoteEnum(int index, String name) {
        this.index = index;
        this.name = name;
    }

    @Override
    public int getIndex() {
        return index;
    }

    @Override
    public String getName() {
        return name;
    }

    public static String getNameByIndex(int index){
        for(RemoteEnum e : RemoteEnum.values()){
            if(e.getIndex() == index){
                return e.getName();
            }
        }
        return null;
    }

    public static RemoteEnum getByIndex(int index){
        for(RemoteEnum e : RemoteEnum.values()){
            if(e.getIndex() == index){
                return e;
            }
        }
        return null;
    }
}
