package org.suze.framework.base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 值对像转化抽像类，目的将源对像转化为值对像。
 * @Date : 2019/6/1 上午8:27
 * @Author : 石冬冬-Seig Heil
 * @param <R> 目标值对像
 * @param <O> 源对像
 */
public abstract class BaseConverter<R extends Serializable, O extends Serializable> {
	/**
	 * 勾子方法，由了子类实现具体的转换规则。
	 * @param o 源对像
	 * @return <R> 目标值对像
	 */
	public abstract R convert(O o);
	/**
	 * List转化方法
	 * @param es 源对像列表
	 * @return List<V>目标值对像列表
	 */
	public List<R> convertList(List<O> es) {
		if(null == es || es.isEmpty()){
			return Collections.emptyList();
		}
		List<R> voList = new ArrayList<>(es.size());
		for(O origin : es) {
			R vo = convert(origin);
			voList.add(vo);
		}
		return voList;
	}
}
