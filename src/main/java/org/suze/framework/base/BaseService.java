package org.suze.framework.base;


import org.suze.framework.base.page.PageForm;

import java.io.Serializable;
import java.util.List;

/**
 * BaseService
 * @Date : 2019/5/31 下午2:28
 * @Author : 石冬冬-Seig Heil
 * @param <E> 数据库实体对象
 * @param <P> 查询主键
 * @param <F> 查询条件Form对象
 */
public interface BaseService<E extends Serializable,P,F extends Serializable> {
    /**
     * 分页加载数据
     * @param form
     * @return
     */
    List<E> queryByPage(PageForm<F> form);
    /**
     * 分页查询数量
     * @param form
     * @return
     */
    int queryCount(PageForm<F> form);
    /**
     * 查询实体对象
     * @param primaryKey
     * @return
     */
    E queryRecord(P primaryKey);
    /**
     * 新增实体对象
     * @param record
     * @return
     */
    int insertRecord(E record);
    /**
     * 修改实体对象
     * @param record
     * @return
     */
    int updateRecord(E record);

    /**
     * 修改实体对象
     * @param record
     * @return
     */
    int updateByPrimaryKeySelective(E record);
    /**
     * 删除对象
     * @param primaryKey
     * @return
     */
    int deleteRecord(P primaryKey);
    /**
     * 批量新增
     * @param list
     * @return
     */
    int batchInsert(List<E> list);

    /**
     * 批量插入(没有即插入，有的话不做处理)
     * @param list
     * @return
     */
    int batchInsertIgnore(List<E> list);
    /**
     * 批量删除
     * @param list
     * @return
     */
    int batchDelete(List<E> list);
    /**
     * 根据条件删除
     * @param form
     * @return
     */
    int deleteByForm(F form);
    /**
     * 查询列表
     * @param form
     * @return
     */
    List<E> queryList(F form);
}
