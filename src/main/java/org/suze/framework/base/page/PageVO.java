package org.suze.framework.base.page;

import java.io.Serializable;
import java.util.List;

/**
 * Description:分页查询VO类 <br/>
 * @author 石冬冬
 * @version V1.0  by 石冬冬-Heil Hitler on  2020/3/27 12:57
 */
public class PageVO<T> implements Serializable {
    private static final long serialVersionUID = 1724063683524348852L;
    /**
     * 总条数
     */
    private int recordsTotal;
    /**
     * 结果集
     */
    private List<T> data;

    public PageVO() {
    }

    public PageVO(int total, List<T> data){
        this.data = data;
        this.recordsTotal = total;
    }
    /**
     * 静态方法，返回实例对象
     * @param total 总条数
     * @param data 数据记录集合
     * @param <T>
     * @return
     */
    public static <T> PageVO newInstance(int total, List<T> data){
        return new PageVO(total,data);
    }

    public PageVO(List<T> data){
        this.data = data;
    }

    public int getRecordsTotal() {
        return recordsTotal;
    }

    public void setRecordsTotal(int recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}
