package org.suze.framework.base;

import org.suze.framework.base.page.PageForm;
import org.suze.framework.base.page.PageVO;

import java.io.Serializable;

/**
 * @description: Facade 接口基类
 * @Date : 2019/5/31 下午2:45
 * @Author : 石冬冬-Seig Heil
 * @param <V> 页面显示VO对象
 * @param <E> 数据库实体对象
 * @param <P> 查询主键
 * @param <F> 查询条件Form对象
 */
public interface BaseFacade<V extends Serializable,E extends Serializable,P,F extends Serializable> {
    /**
     * 分页加载数据
     * @param form
     * @return
     */
    Result<PageVO<V>> loadRecords(PageForm<F> form);
    /**
     * 查询实体对象
     * @param primaryKey
     * @return
     */
    Result<V> queryRecord(P primaryKey);
    /**
     * 保存实体对象
     * @param record
     * @return
     */
    Result<String> saveRecord(E record);
    /**
     * 删除对象
     * @param primaryKey
     * @return
     */
    Result<String> deleteRecord(P primaryKey);

    /**
     * 更新状态
     * @param record
     * @return
     */
    Result<String> modifyStatus(E record);
}
